import unittest
import first_learning_loops




class TestFruits(unittest.TestCase):

    def test_favorite_fruit(self):

        test_fruits = ['kiwi', 'papaya', 'pineapple', 'dragonfruit', 'acai']
        test_favorite_fruit = 'acai'

        test_result = first_learning_loops.find_fruits(test_favorite_fruit,test_fruits)
        self.assertTrue(test_result == test_favorite_fruit)



