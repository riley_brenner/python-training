

# purpose: we have a collection of points on a grid, we want to find the two points that are closest together.

import math

def shortest_distance(x1, x2, y1, y2):
    d = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    return d

print shortest_distance(2,3,2,3)


points = [(2.4, 1.5), (3.5, 2.2), (1, 3), (1.7, 2.1), (2, 2), (3, 3), (4, 1), (1.1, 4)]

point1 = None
point2 = None
for i in range (0, len(points)):
    for j in range (i+1, len(points)):
        distance = shortest_distance(points[i][0], points[j][0], points[i][1], points[j][1])
        if i == 0:
            temp_distance = 0

        if distance > temp_distance:
            temp_distance = distance