#!/usr/bin/python
########################
# TITLE: problem_of_the_boards_2019-08_02
# AUTHOR: russell lego
# DATE: 2019-08-02
# PURPOSE: To show how simple for loops can chose the board with the longest length
# The problem of the boards.
#
# Two contractors are trying to finish a house  To complete the roof, they need a very long wood piece of 2 by 4.
# They have the measurements of all the pieces of wood in their database and they just need get the ID of the piece of wood that is the longest.
########################


#################################
# Importing Modules
#################################


#################################
# Defining Constants
#################################
wood_lengths = [12, 21, 13, 14.5, 16, 19, 24, 45, 22, 7, 0]


#################################
# Defining Functions
#################################

def find_longest_piece(input_array):
    biggest_length_index = None
    biggest_length = 0
    for i in range(0, len(input_array)):
        if input_array[i] > biggest_length:
            biggest_length = input_array[i]
            biggest_length_index = i

    return biggest_length_index


#################################
# Performing Work
#################################
index_of_longest_piece = find_longest_piece(wood_lengths)

message = 'The biggest piece is at index INDEX_REPLACE and has length of LENGTH_REPLACE'
message = message.replace('INDEX_REPLACE', str(index_of_longest_piece))
message = message.replace('LENGTH_REPLACE', str(wood_lengths[index_of_longest_piece]))

print(message)

